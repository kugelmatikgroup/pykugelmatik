import json
from typing import Union


class Command:
    SEND_HOME = 0
    SPHERE_DOWN = 1
    SPHERE_UP = 2
    STOP = 3


class Package:
    """
    Basic class to build a package with instructions for the Kugelmatik

    Parameter:
        cmd :type [Command, int], optional
            The command to execute.
        coordinate :type list(list(int)), optional
            The coordinates of the sphere where the command is executed.
        height :type int, optional
            The height value of the spheres to add or subtract

    Methods:
        get_json_str()
            :returns str with JSON content
        get_json()
            :returns dict if JSON is valid
    """

    def __init__(self, cmd: Union[Command, int] = None,
                 coordinates: list = None,
                 height: int = None):
        self.cmd = cmd
        self.coordinates = coordinates
        self.height = height

    def get_json_str(self) -> str:
        return '{' + \
               f'"cmd":{self.cmd}, ' + \
               f'"coordinates":{self.coordinates if self.coordinates else [[]]}, ' + \
               f'"height":{0 if self.height is None else self.height}' + \
               '}<END>'

    def get_json(self) -> dict:
        try:
            return json.loads(self.get_json_str())
        except json.decoder.JSONDecodeError:
            print("ERROR: Content must be in JSON format.")
