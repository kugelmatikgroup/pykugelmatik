import socket
from threading import Thread

class Server(Thread):
    def __init__(self):
        super(Server, self).__init__(target=self._listen)
        self.host = "localhost"
        self.port = 4840
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.host, self.port))
        self.socket.listen(5)

    def _listen(self):
        while True:
            client, address = self.socket.accept()
            msg = client.recv(1024)
            print(f"Connection from {address}")
            print(f"Seine nachricht: {msg.decode('utf-8')}")

Server().start()