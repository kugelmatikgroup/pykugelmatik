import sys
from datetime import datetime

import cv2
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from camera.gesture import GestureRecognition


class Color:
    WHITE = "#ffffff"
    DARK_GRAY = "#424949"
    GRAY = "#4D5656"
    LIGHT_GRAY = "#717D7E"


style = """
    Window {
        background: #303030;

    }
    QPushButton, QComboBox{
        box-shadow:inset 0px -5px 7px 0px #29bbff;
        background:linear-gradient(to bottom, #2dabf9 5%, #0688fa 100%);
        background-color:#2E86C1;
        border-radius:3px;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Verdana;
        font-size:15px;
        padding:8px 24px;
        text-decoration:none;
        text-shadow:0px 1px 0px #263666;
    }
    QLabel {
        background-color: #4D5656;
        border: 1px solid #262626;
        font: bold 14px;
    }
"""


class Window(QWidget):
    min_value = 0
    max_value = 0
    cam_on = False
    WIDTH = 720
    HEIGHT = 480

    # constructor
    def __init__(self):
        super().__init__()
        # Widgets
        self.cam_switcher = QComboBox(self)
        self.brightness_slider = QSlider(Qt.Vertical)
        self.min_input = QLineEdit(self)
        self.max_input = QLineEdit(self)
        self.cam_label = QLabel("Camera deactivated", self)
        self.inputs_label = None

        # Values
        self.brightness_value_now = 0

        self.initMe()

    def initMe(self):
        print(f"[{datetime.now().strftime('%H:%M:%S')}] program started")
        QToolTip.setFont(QFont("Arial", 14))



        # set cam
        self.cam_switcher.addItem("cam 1")
        self.cam_switcher.addItem("cam 2")
        self.cam_switcher.addItem("cam 3")
        self.cam_switcher.addItem("image")
        self.cam_switcher.currentIndexChanged.connect(self.set_cam)

        # slider
        self.brightness_slider.valueChanged['int'].connect(self.brightness_value)

        # integer inputs
        minButton = QPushButton("set min", self)
        minButton.setFixedSize(100, 30)
        minButton.clicked.connect(self.set_value)

        maxButton = QPushButton("set max", self)
        maxButton.setFixedSize(100, 30)
        maxButton.clicked.connect(self.set_value)

        self.min_input.setFixedSize(100, 30)
        self.min_input.setValidator(QIntValidator())
        self.min_input.setText("0")
        self.min_input.setMaxLength(3)

        self.max_input.setFixedSize(100, 30)
        self.max_input.setValidator(QIntValidator())
        self.max_input.setText("0")
        self.max_input.setMaxLength(3)

        # start/stop button
        button = QPushButton("Start", self)
        button.setStyleSheet("background-color: #28B463")
        button.setToolTip("ToolTipTest")
        button.clicked.connect(self.pressed)

        # color picker
        color_picker = QPushButton("color picker", self)
        color_picker.clicked.connect(self.color_picker)

        # camer window
        self.cam_label.setAlignment(Qt.AlignCenter)
        self.inputs_label = QLabel(f"min: {Window.min_value}\n"
                                   f"max: {Window.max_value}", self.cam_label)
        self.inputs_label.setStyleSheet("border: 0px, opacity: 0")

        # layout
        pagelayout = QHBoxLayout()
        input_layout = QVBoxLayout()
        cam_layout = QVBoxLayout()

        pagelayout.addLayout(input_layout)
        pagelayout.addLayout(cam_layout)

        cam_layout.addWidget(self.cam_label)
        cam_layout.addWidget(button)
        cam_layout.addWidget(color_picker)

        min_layout = QHBoxLayout()
        min_layout.addWidget(self.min_input)
        min_layout.addWidget(minButton)

        max_layout = QHBoxLayout()
        max_layout.addWidget(self.max_input)
        max_layout.addWidget(maxButton)

        input_layout.addWidget(self.brightness_slider)
        input_layout.addWidget(self.cam_switcher)
        input_layout.addLayout(min_layout)
        input_layout.addLayout(max_layout)

        self.setLayout(pagelayout)

        # main window
        self.setGeometry(50, 50, 1000, 500)
        self.setWindowTitle('GUI Kugelmatik | V1')
        self.setWindowIcon(QIcon("KKS_icon.png"))
        self.show()
        self.setStyleSheet(style)

        print(f"[{datetime.now().strftime('%H:%M:%S')}] Main class executed")

    def set_cam(self):
        if Window.cam_on:
            self.cam_label.setText(self.cam_switcher.currentText())

            if self.cam_switcher.currentText() == "cam 1":
                image = GestureRecognition.currentFrame
                print(type(image))
                if image is not None:
                    #resizing img to cam_label. Subtracting 2 because of the border
                    image = cv2.resize(image, (self.cam_label.width()-2, self.cam_label.height()-2))
                    height, width, channel = image.shape
                    bytesPerLine = 3 * width
                    image = QPixmap(QImage(image, width, height, bytesPerLine, QImage.Format_RGB888).rgbSwapped())
                    self.update_image(image)

            #if self.cam_switcher.currentText() == "image":
            #    image = QPixmap('KKS_icon.png')
            #    self.update_image(image)

    def brightness_value(self, value):
        self.brightness_value_now = value
        print('Brightness: ', value)

    def update_image(self, image):
        print(type(image))
        self.cam_label.setPixmap(image)

    def get_pixel(self, event):
        x = event.pos().x()
        y = event.pos().y()
        print(str(x) + " " + str(y))
        # c = self.cam.pixel(x, y)  # color code (integer): 3235912
        # depending on what kind of value you like (arbitary examples)
        # c_rgb = QColor(c).getRgb()  # 8bit RGBA: (255, 23, 0, 255)
        # print(c_rgb)

    def set_value(self):
        sender = self.sender()
        if sender.text() == "set min":
            Window.min_value = self.min_input.text() if self.min_input.text() else 0
            print(f"Set min: {Window.min_value}")
        elif sender.text() == "set max":
            Window.max_value = self.max_input.text() if self.max_input.text() else 0
            print(f"Set max: {Window.max_value}")

        self.inputs_label.setText(f"min: {Window.min_value}\n"
                                  f"max: {Window.max_value}")

    def color_picker(self):
        color = QColorDialog.getColor()
        print(color.name())
        # self.cam.mousePressEvent = self.get_pixel

    def pressed(self):
        sender = self.sender()

        if sender.text() == "Start":
            sender.setText("Stop")
            sender.setStyleSheet("background-color: #B03A2E")
            Window.cam_on = True
            Window.set_cam(self)

        elif sender.text() == "Stop":
            sender.setText("Start")
            sender.setStyleSheet("background-color: #28B463")
            self.cam_label.setText("camera deactivated")
            Window.cam_on = False

        print(f"[{datetime.now().strftime('%H:%M:%S')}] button pressed")


app = QApplication(sys.argv)
w = Window()
sys.exit(app.exec_())

