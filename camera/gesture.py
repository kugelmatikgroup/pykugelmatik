import sys

import cv2
import cvzone.HandTrackingModule
import numpy as np
from pathlib import Path
from threading import Thread
from cvzone.HandTrackingModule import HandDetector
import mediapipe

from PyKugelmatikAPI.api import APIHandler
from PyKugelmatikAPI.data import *

handler = APIHandler()


class VideoStream:
    """
    A class used to open a video stream and display it.

    Parameter:
        **kwargs
            video_path :type str, optional
                A valid path to a streamable video file
            cam_index :type int, optional
                The index of a webcam.
    Methods:
        start(window_name='Capture')
            Starts the main loop and creates a stream window.
        stop()
            Stops the stream and destroys all windows.

    Raises:
        AttributeError
            If neither video_path or cam_index is not given.
            If video path is not valid.
            If stream couldn't be opened (invalid cam index or no streamable video file)

    """

    def __init__(self, **kwargs):
        video_path = kwargs.get("video_path")
        cam_index = kwargs.get("cam_index")

        if video_path is None and cam_index is None:
            raise AttributeError("There must be a video path or a cam index given.")
        if video_path is not None and not Path(str(video_path)).exists():
            raise AttributeError(f"The video path '{video_path}' isn't valid.")

        if video_path is not None:
            self.capture = cv2.VideoCapture(str(video_path))
        else:
            self.capture = cv2.VideoCapture(cam_index)

        if self.capture is None or not self.capture.isOpened():
            raise AttributeError("Stream couldn't be opened.")

        self.capture_is_running = False
        self.window_name = "Capture"

    # TODO callback in init?!
    def start(self, callback, window_name="Capture"):
        self.window_name = window_name
        self.capture_is_running = True
        thread = Thread(target=self.__main_loop, args=(callback,))
        thread.start()

    def stop(self, ):
        self.capture_is_running = False

    def _destroy_window(self):
        cv2.destroyWindow(self.window_name)
        print("Destroying Windows...")

    def __main_loop(self, callback):
        while True:
            frame_is_valid, frame = self.capture.read()

            if frame_is_valid:
                cv2.imshow(self.window_name, callback(frame))
            elif frame is None:
                self._destroy_window()
                break

            if cv2.waitKey(1) == 27 or not self.capture_is_running:
                self._destroy_window()
                break


class GestureRecognition(VideoStream):
    currentFrame = None

    def __init__(self, video_path: str = None, cam_index: int = None):
        super(GestureRecognition, self).__init__(video_path=video_path, cam_index=cam_index)
        self.background_subtractor = cv2.createBackgroundSubtractorMOG2(varThreshold=10)

        self.lower = np.array([7, 26, 176], dtype="uint8")
        self.higher = np.array([144, 173, 223], dtype="uint8")
        self.minimumArea = 1000
        self.maximumArea = 10000
        self.minimumMovementSpeed = 35
        self.maximumMovementSpeed = 100
        self.buffer = []
        self.lastPos = 0
        self.lastHand1Pos = [0, 0]
        self.lastHand2Pos = [0, 0]

    def start_recognition(self):
        self.start(self.analyze_frame)

    def stop_recognition(self):
        self.stop()

    def analyze_frame(self, frame):
        frame = cv2.resize(frame, (720, 480))

        detector = HandDetector()
        hands, frame = detector.findHands(frame)
        
        averageHandPos = []
        if hands:
            hand1 = hands[0]
            hand1Pos = hand1['center']
            moveAmount = hand1Pos[1] - self.lastHand1Pos[1]
            self.lastHand1Pos = hand1Pos

            if len(hands) == 2:
                hand2 = hands[1]
                hand2Pos = hand2['center']
                moveAmount += hand2Pos[1] - self.lastHand2Pos[1]
                self.lastHand2Pos = hand2Pos

            WRIST = list(dict(hand1).values())[0][0]
            THUMB_CMC = list(dict(hand1).values())[0][1]
            THUMB_MCP = list(dict(hand1).values())[0][2]
            THUMB_IP = list(dict(hand1).values())[0][3]
            THUMB_TIP = list(dict(hand1).values())[0][4]
            INDEX_FINGER_MCP = list(dict(hand1).values())[0][5]
            INDEX_FINGER_PIP = list(dict(hand1).values())[0][6]
            INDEX_FINGER_DIP = list(dict(hand1).values())[0][7]
            INDEX_FINGER_TIP = list(dict(hand1).values())[0][8]
            MIDDLE_FINGER_MCP = list(dict(hand1).values())[0][9]
            MIDDLE_FINGER_PIP = list(dict(hand1).values())[0][10]
            MIDDLE_FINGER_DIP = list(dict(hand1).values())[0][11]
            MIDDLE_FINGER_TIP = list(dict(hand1).values())[0][12]
            RING_FINGER_MCP = list(dict(hand1).values())[0][13]
            RING_FINGER_PIP = list(dict(hand1).values())[0][14]
            RING_FINGER_DIP = list(dict(hand1).values())[0][15]
            RING_FINGER_TIP = list(dict(hand1).values())[0][16]
            PINKY_MCP = list(dict(hand1).values())[0][17]
            PINKY_PIP = list(dict(hand1).values())[0][18]
            PINKY_DIP = list(dict(hand1).values())[0][19]
            PINKY_TIP = list(dict(hand1).values())[0][20]

            if moveAmount > 100:
                handler.move_all_down(moveAmount * 5)
            if moveAmount < 100:
                handler.move_all_up(-moveAmount * 5)

            """
            if hand1:
                
                if (landmark9[1] - landmark12[1]) < 0:
                    posDataList = []
                    for clusterX in range(0, 5):
                        for clusterY in range(0, 5):
                            for sphereX in range(0, 5):
                                for sphereY in range(0, 6):
                                    posDataList.append(PosData([clusterX, clusterY, sphereX, sphereY], 0))
                    handler.set_spheres(posDataList)
        """

        self.currentFrame = frame
            
        """
        # mask = cv2.cvtColor(frame, cv2.COLOR_)
        mask = cv2.inRange(frame, self.lower, self.higher)
        mask = cv2.medianBlur(mask, 5)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (8, 8))
        mask = cv2.dilate(mask, kernel)

        contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        filteredContours = []
        for i in contours:
            if cv2.contourArea(i) > self.minimumArea and cv2.contourArea(i) < self.maximumArea:
                filteredContours.append(i)
        frame = cv2.drawContours(frame, filteredContours, -1, (0, 255, 0), -1)

        averageX = 0
        averageY = 0
        length = 0
        for i in filteredContours:
            M = cv2.moments(i)
            if M['m00'] != 0:
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
                cv2.circle(frame, (cx, cy), 7, (0, 0, 255), -1)
                averageX += cx
                averageY += cy
                length += 1

        if length > 0:
            averageX = int(averageX / length)
            averageY = int(averageY / length)
            if averageX - self.lastPos > self.maximumMovementSpeed and self.lastPos - averageX > self.maximumMovementSpeed:
                averageX = self.lastPos
            cv2.circle(frame, (averageX, averageY), 15, (255, 0, 0), -1)

            if len(self.buffer) < 5:
                self.buffer.append(averageX - self.lastPos)
                self.lastPos = averageX
            moveAmount = sum(self.buffer, 0) / len(self.buffer)
            if moveAmount > 0:
                handler.move_all_down(moveAmount * 3)
            else:
                handler.move_all_up(moveAmount * -3)
            self.buffer.clear()
            """
        return frame